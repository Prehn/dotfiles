# slp's dotfiles
A few of my most treasured dotfiles. I like fiddling with dotfiles and change
my programs to fit my own needs and wants, although I still have a lot to learn.

There is no WARRANTY if you choose to copy any of my dotfiles, but I'm always
open to criticism and good ideas, so feel free to throw them my way.
