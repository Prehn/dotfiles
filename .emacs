(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
   (quote
    ("b46ee2c193e350d07529fcd50948ca54ad3b38446dcbd9b28d0378792db5c088" default)))
 '(display-line-numbers-type (quote relative))
 '(display-time-mode t)
 '(elfeed-feeds
   (quote
    ("https://mullvad.net/en/blog/feed/rss" "https://jeff-vogel.blogspot.com/feeds/posts/default" "https://harryrschwartz.com/atom.xml" "https://static.fsf.org/fsforg/rss/news.xml" "www.bay12games.com/dwarves/dev_release.rss" "www.bay12games.com/dwarves/dev_now.rss" "https://nullprogram.com/feed/")))
 '(menu-bar-mode nil)
 '(org-agenda-files (quote ("~/calendar.org")))
 '(package-archives
   (quote
    (("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa-stable" . "https://stable.melpa.org/packages/"))))
 '(package-selected-packages (quote (elfeed dracula-theme org magit fsharp-mode)))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
;; elfeed keybinding
(global-set-key (kbd "C-x w") 'elfeed)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Input" :foundry "FBI " :slant normal :weight normal :height 113 :width semi-condensed)))))
