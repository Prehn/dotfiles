"""""""""""""""""""""""""""""""""""""""""""""""""
" URL: https://gitlab.com/Prehn/dotfiles        "
" Author: Sebastian Larsen Prehn                "
" Description: Personal .vimrc file             "
" Other Contributors:                           "
" - Tórur Zachariasen                           "
" Contributed many lines to this .vimrc through "
" showcase of his .vimrc and healthy discussion."
""""""""""""""""""""""""""""""""""""""""""""""""" 

"""""""""""""""""""""""""""""""""""""""""""""""""
" TABLE OF CONTENTS                             "
" * Plugins                                     "
" * General                                     "
" * Remapping                                   "
" * Themes, Colours and Fonts                   "
" * User Interface                              "
" * Files, Backups and Undo                     "
" * Function                                    "
"""""""""""""""""""""""""""""""""""""""""""""""""

" * Plugins *

" Automatic installation of vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


set nocompatible

" start vim-plug
call plug#begin('~/.vim/vim-plugins')

" Vim-plug : enables help from within vim for vim-plug (:help plug-options)
Plug 'junegunn/vim-plug'

" vim-airline : status/tabline + themes
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/vim-airline/vim-airline-themes.git'

" tmuxline : simple tmux statusline generator
Plug 'https://github.com/edkolev/tmuxline.vim.git'

" ctrlp : full path fuzzy file, buffer, mru, tag, ... finder for vim
Plug 'ctrlpvim/ctrlp.vim'

" Syntastic : syntax checking plugin
Plug 'vim-syntastic/syntastic'

" vim-fsharp : F# support for Vim
Plug 'fsharp/vim-fsharp', {
    \ 'for': 'fsharp',
    \ 'do': 'make fsautocomplete',
    \}

" Dracula theme for Vim
Plug 'dracula/vim',{'as':'dracula'}

" Goyo : Distraction-free writing in Vim
Plug 'junegunn/goyo.vim'

" vim-pencil : Writing tool for Vim
Plug 'reedes/vim-pencil'

" vim-gitgutter : git tool that shows git diff in the gutter
Plug 'airblade/vim-gitgutter'

" Initialize plugin system
call plug#end()

" Remember to :PlugInstall whenever you add a new plugin!

"""""""""""""""""""""""""""""""""""""""""""""""""
" * General *

" Set the length of the command line history
set history=500

" Enable file types
filetype plugin indent on

" Change Leader Key
let mapleader = "\<space>"

" Line numbers
set rnu
set nu

" Enable mouse
set mouse=a

" Set tabs width to 4 spaces
set tabstop=4
set shiftwidth=4

" Use spaces instead of tabs
set expandtab

" Set a vertical line at column 80 : For better coding practice
set colorcolumn=80
highlight ColorColumn ctermbg=DarkRed
highlight ColorColumn guibg=DarkRed

" Set the Default Encoding
set encoding=utf8

"""""""""""""""""""""""""""""""""""""""""""""""""
" * Remapping *

" Quick Search
nnoremap <space><space> /

" Quick Save
nnoremap <leader>w :w<cr>

" Quick Quit
nnoremap <leader>q :q<cr>

" Quick Save and Quit
nnoremap <leader>sq :wq<cr>

" Open netrw
nnoremap <leader>e :Vexplore<cr>

" LaTeX compilation
nnoremap <leader>t :call TexToPDF()<cr>

" FSharp compilation
nnoremap <leader>ft :call FsharpCompile()<cr>

" FSharp Interactive
nnoremap <leader>fi :call FsharpInteractive()<cr>

" Remapping jj as a escape character 
inoremap jj <esc>

"""""""""""""""""""""""""""""""""""""""""""""""""
" * Themes, Colors and Fonts *

" Enable Syntax Highlighting
syntax on

" Enable 256 Colors Palette
set t_Co=256

"""""""""""""""""""""""""""""""""""""""""""""""""
" * User Interface *

" Turn on the Wild Menu
set wildmenu

" Show trailing whitespace and spaces before a tab
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/

" Make Backspace behave normally
set backspace=indent,eol,start

" Move over linewraps
set whichwrap+=<,>,h,l,[,]

" Ignore case when searching
set ignorecase

" Use case sensitive when a pattern contains an upper letter
set smartcase

" Highlight Search results
set hlsearch

" Incremental Search
set incsearch

" Don't redraw while executing Macros
set lazyredraw

" netrw settings
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3
let g:netrw_altv = 1
let g:netrw_winsize = 25

"""""""""""""""""""""""""""""""""""""""""""""""""
" * Files, Backups and Undo *

"""""""""""""""""""""""""""""""""""""""""""""""""
" * Functions *

" Create a PDF from an opened TeX file
function! TexToPDF()
    :!pdflatex %:t
endfunction

" Compile Fsharp code
function! FsharpCompile()
    :!fsharpc %:t 
endfunction

" Fsharp Interactive
function! FsharpInteractive()
    :!fsharpi %:t
endfunction
